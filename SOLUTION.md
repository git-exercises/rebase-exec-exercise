We could do:

    git rebase -i HEAD~4

Then insert after each "pick" a line:

    exec ./test.py

But git can do it itself with:

    git rebase -i --exec ./test.py HEAD~4

git will stop when the exec command fails, i.e. when the test fails.
