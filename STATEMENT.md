In the current state of the "main" branch, the unit tests fail.
Don't try to debug what fails now, don't look at `foo.py`, only care about the tests.
Instead, find out with `git rebase` and the `exec` instruction in git-rebase when the regression was introduced.
Then, use `git rebase --abort` to reset the initial branch as it was.

To run the unit tests, simply run `./test.py`.
